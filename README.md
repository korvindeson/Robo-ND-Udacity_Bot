# Robo-ND-Udacity_Bot
Localize robot in 3D environment

# Included
Package prepared for ROS. Files describing the robot and simple world to test it.

# Legacy files
Files:

 - amcl_for_original_model.launch
 
 - udacity_bot_backup_original.xacro

Describe the basic model provided by Udacity and amcl settings for it.

# Requred
ros-kinetic-navigation
ros-kinetic-map-server
ros-kinetic-move-base
rospack profile
ros-kinetic-amcl

